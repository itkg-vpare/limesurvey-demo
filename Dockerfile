FROM tutum/lamp

RUN apt-get update ; \
	apt-get upgrade -q -y ; \
	apt-get install -q -y curl php5-gd php5-ldap php5-imap; \
	apt-get install -q -y ssmtp && echo "sendmail_path = /usr/sbin/ssmtp -t" > /etc/php5/mods-available/z_sendmail.ini && php5enmod z_sendmail && echo "mailhub=mail:25\nUseTLS=NO\nFromLineOverride=YES" > /etc/ssmtp/ssmtp.conf; \
    apt-get clean ; \
	php5enmod imap
	
RUN chown -R www-data:www-data /app

RUN chown www-data:www-data /var/lib/php5

ADD apache_default /etc/apache2/sites-available/000-default.conf

EXPOSE 80 3306
CMD ["/run.sh"]
